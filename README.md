# spring-boot-kafka
A simple application to Produce and Consume a message using [Kafka](https://kafka.apache.org/) with Springboot. The message here is a JSON Object of a User class.

## What is ZooKeeper
Zookeeper is used to track cluster state, membership, and leadership.  For example:

1. Zookeeper keeps track of which brokers are part of the Kafka cluster
2. Zookeeper is used by Kafka brokers to determine which broker is the leader of a given partition and topic and perform leader elections
3. Zookeeper stores configurations for topics and permissions
4. Zookeeper sends notifications to Kafka in case of changes (e.g. new topic, broker dies, broker comes up, delete topics, etc.…)

![Zookeeper Architecture](https://static.javatpoint.com/tutorial/kafka/images/apache-kafka-architecture2.png "Zookeeper Architecture")

## What is Kafka
Apache Kafka is an event streaming platform. Kafka combines three key capabilities
1. To publish (write) and subscribe to (read) streams of events, including continuous import/export of your data from other systems.
2. To store streams of events durably and reliably for as long as you want.
3. To process streams of events as they occur or retrospectively.

And all this functionality is provided in a distributed, highly scalable, elastic, fault-tolerant, and secure manner. An **event** records the fact that "something happened" in the world or in your business. When you read or write data to Kafka, you do this in the form of events.  
**Producers** are those client applications that publish (write) events to Kafka, and **Consumers** are those that subscribe to (read and process) these events. In Kafka, producers and consumers are fully decoupled and agnostic of each other, which is a key design element to achieve the high scalability that Kafka is known for. Events are organized and durably stored in **Topics**. Topics are partitioned, meaning a topic is spread over a number of "buckets" located on different Kafka brokers. This distributed placement of your data is very important for scalability because it allows client applications to both read and write the data from/to many brokers at the same time.

![Kafka Architecture](https://static.javatpoint.com/tutorial/kafka/images/apache-kafka-architecture3.png "Kafka Architecture")

## Difference between RabbitMQ and Kafka
RabbitMQ is an open source message broker written in Java. It's fully compliant with JMS 1.1 standards. JMS is a specification that allows development of message based system. RabbitMQ acts as a broker of messages which sits in between applications and allows them to communicate in asynchronous and reliable way.
There are two types of messaging options.
1. Point to Point
2. Publish/Subscribe

**Point to Point**

In this type of communication, the broker sends messages to only one consumer, while the other consumers will wait till they get the messages from the broker. No consumer will get the same message. This type of communication is also called as Queue based communication where the Producer sends messages to a queue and only one consumer gets one message from the queue. If there is more than one consumer, they may get the next message but they won’t get the same message as the other consumer.

![Point to Point](https://www.tutorialspoint.com/rabbitmq/images/point_to_point_messaging.jpg "Point to Point")

**Publish/Subscribe**

In this type of communication, the Broker sends same copy of messages to all the active consumers. This type of communication is also known as Topic based communication where broker sends same message to all active consumer who has subscribed for particular Topic. This model supports one-way communication where no verification of transmitted messages is expected.

![Publish/Subscribe](https://www.tutorialspoint.com/rabbitmq/images/publish_subscribe_messaging.jpg "Publish/Subscribe")

### Difference

| Features | Kafka | RabbitMQ |
|----------|-------|----------|
| **Message retention/deletion** | Messages are deleted once the retention period is over | Right after consumers receives the message it's deleted |
| **Message replay** | Yes, possible under the retention period | No, since messages are deleted off the queue promptly after delivery |
| **Routing Available** | Doesn’t have routing algorithms/rules. Topics define the necessary segregation | Comes with many complex routing rules. Exchange and binding is used to push messages to appropriate queues |
| **Priority** | No in-built priority feature | Priority queues are implemented in place |
| **Architecture** | Producer -> exchange -> binding rules -> queue -> consumer | Producer -> topic wise -> broker -> partition -> consumer |
| **Use case**  | Operational - process operation, logging | Transactional - e.g. order formation |
| **Message format** | Key-value pair, timestamp | Header and body |
| **Maintaining sequential Order** | Kafka maintains offset to keep the order of arrival of messages intact | RabbitMQ implicitly uses Queue that follows the FIFO property and thus keeps proper order of messages |

### Start ZooKeeper
```
./zookeeper-server-start.sh ../config/zookeeper.properties
```

### Start Kafka
```
./kafka-server-start.sh ../config/server.properties
```

### Create a Topic
```
./kafka-topics.sh --bootstrap-server localhost:9092 --create --topic MyTopic
```

### Produce a message through CLI
```
./kafka-console-producer.sh --bootstrap-server localhost:9092 --topic MyTopic
```

### Consume a message through CLI
```
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic MyTopic --from-beginning
```

### Maven dependency
```xml
	<dependency>
		<groupId>org.springframework.kafka</groupId>
		<artifactId>spring-kafka</artifactId>
	</dependency>
```

### Build the Project skipping Tests
```
mvn clean install -DskipTests
```

### Run the Project
```
java -jar spring-boot-kafka-0.0.1-SNAPSHOT.jar
```

### Usage
POST request to `http://localhost:8080/publish` to publish a message to topic "MyTopic"

```json
{
    "firstName": "John",
    "lastName": "Doe",
    "emailId": "john.doe@gmail.com",
    "age": 35
}
```