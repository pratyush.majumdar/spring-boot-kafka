package com.pratyush.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pratyush.kafka.model.User;

@RestController
public class UserController {
	
	@Autowired
	KafkaTemplate<String, User> kafkaTemplate;
	
	private static final String TOPIC = "MyTopic";
	
	@PostMapping("/publish")
	public String publishMessage(@RequestBody User user) {
		kafkaTemplate.send(TOPIC, user);
		return "Publish Successfully !!!";
	}
}
